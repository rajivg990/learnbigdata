import org.apache.spark.sql.SparkSession

object readCsvFile extends App {

    val spark = SparkSession.builder().
      appName("Read Csv File").
      config("spark.master", "local").
      getOrCreate()

  spark.sparkContext.setLogLevel("ERROR")

  //Using Header as true, will read the first line of a file as a header.
  //Using Inferschema as true, Spark will read the file and give the correct datatype of the file. Otherwise it will give all the column datatype as a string only.
  val readCsv = spark.read.option("header", "true").csv("/Users/roohi/Desktop/Code/learnbigdata/src/main/files/readCsv.csv")
  readCsv.show()
  readCsv.printSchema()

  val readCsv_inferschema = spark.read.option("header", "true").option("inferSchema", "true").csv("/Users/roohi/Desktop/Code/learnbigdata/src/main/files/readCsv.csv")
  readCsv_inferschema.printSchema()



}
