import pandas as pd
import sys

# In this program, we are using python pandas to convert csv file to html file.
# This program will take 2 arguments.
# 1st argument it takes

//csvFile = sys.argv[0]
//outputFile = sys.argv[1]

csvFile = "/Users/roohi/Desktop/Code/pycharm/learnbigdata/src/main/eBay/Workbook2.csv"
outputFile = "/Users/roohi/Desktop/Code/pycharm/learnbigdata/src/main/eBay/Workbook2.html"
#
file_read = pd.read_csv(csvFile, encoding='iso-8859-1')
data_html = file_read.to_html()


with open(outputFile, 'w+') as fh:
    fh.write(data_html)


