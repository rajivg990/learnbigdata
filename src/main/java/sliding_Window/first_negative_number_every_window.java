package sliding_Window;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Deque;
public class first_negative_number_every_window {
    public long[] printFirstNegativeInteger(long A[], int N, int K)
    {
        int i=0;
        int j=0;
        Deque<Long>q=new LinkedList<>();
        ArrayList<Long>a=new ArrayList<>();
        while(j<N){
            if(A[j]<0){
                q.add(A[j]);
            }
            if(j-i+1<K){
                j++;
            }
            else if(j-i+1==K){
                if(!q.isEmpty()){
                    a.add(q.peekFirst());
                    if(q.peekFirst()==A[i]){
                        q.pollFirst();
                    }
                }
                else{
                    a.add((long)0);
                }
                i++;
                j++;
            }

        }
        long ans[]=new long[a.size()];
        for(int aa=0;aa<a.size();aa++){
            ans[aa]=a.get(aa);
        }
        return ans;

    }




    public static void main(String[] args) {
        int arr [] = {12,-1,-7,8,15,30,16,8};
        int k = 3;
        int n = 8;
      //  printFirstNegativeInteger(arr,n,k);

    }
}
