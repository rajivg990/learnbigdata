package sliding_Window;

public class maximum_sum_subarray {

    public static void max_sum_subarray(int arr [] , int k){
        int n = arr.length;
        int i = 0,j = 0,max = Integer.MIN_VALUE ,sum = 0;
        while (j < n){
            sum += arr[j];
            if(j-i + 1 == k){
                max = Math.max(max,sum);
                sum -= arr[i];
                i++;
            }
            j++;
        }
        System.out.println(max);
    }



    public static void main(String[] args) {
        int arr [] = {1,2,6,3};
        int k = 3;
        max_sum_subarray(arr,k);
    }
}
