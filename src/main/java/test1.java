public class test1 {
    static void SearchElementRotatedArray(int arr[],int x){
        int index = 3;
        int start,end;
        if(x == arr[index]) {
            System.out.println("Element Found at index " + index);
            System.exit(0);
        }
        else{
            if(x > arr[arr.length - 1]){
                start = 0;
                end = index - 1;
            }
            else {
                start = index + 1;
                end = arr.length - 1;
            }
            while (start <= end){
                int mid = (start + end) / 2;
                if(arr[mid] == x){
                    System.out.println("Element found at index " + mid);
                    System.exit(0);
                }
                else if(arr[mid] < x){
                    start = mid + 1;
                }
                else {
                    end = mid - 1;
                }

            }

        }
        System.out.println("Element not found");
    }

    public static void main(String[] args) {
        int arr [] = {12,13,14,1,2,3,4};
        int x = 3;
        SearchElementRotatedArray(arr,x);


    }
}
