package backup;
import java.util.HashSet;
import java.util.Set;
public class longSubString {
    static int lengthOfLongestString(String s){
        int maxCount = 0;
        int i = 0;
        int j = 0;
        int strlen = s.length();
        Set<Character> st = new HashSet<>();
        while(i < strlen && j <strlen){
            if(!st.contains(s.charAt(j))){
                st.add(s.charAt(j));
                j++;
                maxCount = Math.max(maxCount,j-i);
               }
            else {
                st.remove(s.charAt(i));
                i++;
            }
        }
        return maxCount;
    }
    public static void main(String[] args) {
        String str = "geeksforgeeks";
        System.out.println(lengthOfLongestString(str));
    }
}
