package backup;

public class moveZoroToEnd {
    static void moveZoroToEnd( int [] arr){
        int count = 0;
        for(int i = 0;i<arr.length ;i++){
            if(arr[i] != 0){
                arr[count] = arr[i];
                count++;
            }
        }
        while (count < arr.length){
            arr[count] = 0;
            count++;
        }
    }

    public static void move(int input[]){
        int i = 0,j=input.length -1;
        while(i<j){
            if(input[i] == 0){
                if(input[j] == 0){
                    j--;
                    return;
                }
                else {
                    int temp = input[i];
                    input[i] = input[j];
                    input[j] = temp;
                    i++;
                    j--;
                }
            }
            else{
                i++;
            }
        }
        for(int k = 0;k <input.length ;k++){
            System.out.print(input[k] + " ");}
    }


    public static void main(String[] args) {
     int arr [] = {1,2,4,0,1,0,5,0,0};
       // moveZoroToEnd(arr);
       move(arr);

    }
}
