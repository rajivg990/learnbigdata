package backup;

import java.util.Arrays;
import java.util.stream.Stream;
public class SortyarrayGroup {

    public static int[] solution(int[] data, int segment){
        int[] finalData = new int[data.length/segment];
        int sum;
        for (int i=0;i<data.length/segment;i++){
            int[] subData = Arrays.copyOfRange(data,i*segment,segment*(i+1));
            sum = 0;
            for(int j = subData.length-1;j>=0;j--){
                sum = (int) (sum+subData[j]*Math.pow(10,segment-j-1));
            }
            finalData[i] = sum;
        }
        return finalData;
    }
    public static void main(String[] args) {
        int[] data = new int[]{6,5,3,2,3,6,2,9,7};
        int segment = 3;
        System.out.println(Arrays.toString(solution(data,segment)));;
    }

}