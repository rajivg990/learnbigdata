package backup;

import java.util.HashSet;

public class firstDuplicateElementArray {
        static void firstDuplicateElementArrayFirst(int arr[]) {
            int temp = 0;
            for(int i = 0;i<arr.length - 1;i++){
                for(int j = i+1;j<arr.length;j++){
                    if(arr[i] == arr[j] && (i != j)){
                        System.out.println("duplicate element is " + arr[i]);
                        temp = temp + 1;
                        break;
                    }
                }
            }
            if(temp == 0){
                System.out.println("No Duplicate Element");
            }
        }
        static void firstDuplicateElementArraySecond(int arr[]){
            HashSet<Integer> hs = new HashSet<>();
            int temp = -1;
            for(int i = arr.length-1;i>= 0;i--){
                if(hs.contains(arr[i])){
                    temp = i;
                }
                else {
                    hs.add(arr[i]);
                }
            }
            if(temp != -1){
                System.out.println("Duplicate element is " + arr[temp]);
            }
            else {
                System.out.println("No Duplicate Element Found");
            }
        }

    public static void main(String[] args) {
            int arr [] = {1,2,4,6,6,8,9,9,2};

      //  firstDuplicateElementArrayFirst(arr);
        firstDuplicateElementArraySecond(arr);


    }
}
