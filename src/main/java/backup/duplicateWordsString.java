package backup;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class duplicateWordsString {
        static void duplicateWordString(String str){
            Map<String,Integer> map = new HashMap<>();
           //To convert String to Char array use below
           // char [] ch = str.toCharArray();
            String arr [] = str.split(" ");
            for(int i = 0; i < arr.length;i++){
                if(map.containsKey(arr[i])){
                    map.put(arr[i],map.get(arr[i]) + 1);
                }
                else{
                    map.put(arr[i],1);
                }
            }
            Iterator<String> itr = map.keySet().iterator();
            while (itr.hasNext()){
                String temp = itr.next();
                if(map.get(temp) > 1){
                    System.out.println("String " + temp + " appered " + map.get(temp) + " times");
                }
            }
        }
    public static void main(String[] args) {
        String str = "Hi My name is Rajiv  Rajiv";
        duplicateWordString(str);
    }
}