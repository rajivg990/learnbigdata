package backup;
public class selectionSort {
    static void selectionSort(int arr[]){
        int min,temp = 0;
        for(int i = 0;i<arr.length;i++){
            min = i;
            for(int j = i + 1; j < arr.length;j++){
                if(arr[j] < arr[min]){
                    min = j;
                }
            }
            temp = arr[i];
            arr[i] = arr[min];
            arr[min] = temp;
        }
    }
    public static void main(String[] args) {
        int arr [] = {1,4,2,8,5,9,6};
        selectionSort(arr);
        for(int i = 0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
    }
}
