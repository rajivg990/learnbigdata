package backup;
import java.util.HashSet;
public class firstDuplicateElementString {
        static void firstDuplicateElementStringFirst(String str){
            int temp = 0;
            for(int i = 0;i<str.length() - 1;i++){
                for(int j=i+1;j<str.length();j++){
                    if(str.charAt(i) == str.charAt(j)){
                        System.out.println("First Duplicate Character is " + str.charAt(i));
                        temp = temp + 2;
                        return;
                    }
                }
            }
            if(temp == 0){
                System.out.println("Duplicate Character Not Found.");
            }
        }
        static void firstDuplicateElementStringSecond(String str){
            int temp = -1;
            HashSet<Character> hs = new HashSet<>();
            for(int i = str.length() -1;i>= 0;i--){
                if(hs.contains(str.charAt(i))){
                    temp = i;
                }
                else {
                    hs.add(str.charAt(i));
                }
            }
            if(temp != -1){
                System.out.println("First Duplicate Element is " + str.charAt(temp));
            }
            else {
                System.out.println("Duplicte Character not found");
                System.out.println(hs);
            }
        }
    public static void main(String[] args) {
            String str = "RRrajjiv";
       // firstDuplicateElementStringFirst(str);
        System.out.println(str.charAt(4) == str.charAt(5));
       // firstDuplicateElementStringSecond(str);
    }
}