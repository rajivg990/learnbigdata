package backup;

import java.util.*;

public class test {
    private static void kSort(int[] arr, int n, int k)
    {
        // min heap
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();

        // add first k + 1 items to the min heap
        for(int i = 0; i < k + 1; i++)
        {
            priorityQueue.add(arr[i]);
        }

        int index = 0;
        for(int i = k + 1; i < n; i++)
        {
            arr[index++] = priorityQueue.peek();
            priorityQueue.poll();
            priorityQueue.add(arr[i]);
        }

        Iterator<Integer> itr = priorityQueue.iterator();

        while(itr.hasNext())
        {
            arr[index++] = priorityQueue.peek();
            priorityQueue.poll();
        }

    }

    public static void main(String[] args) {
      int array [] = {6, 5, 3, 2, 8, 10, 9};
        int n = array.length;
        int k = 3;
       // kSort(array, n, k);
        for(int i = 0; i < array.length ; i++){
           // System.out.println(array[i]);
        }
        String str = "geeksforgeeks";
        Set<Character> st = new HashSet<>();
        for(int i = 0;i<str.length();i++){
            st.add(str.charAt(i));
        }
        System.out.println(st.size());
    }
}
