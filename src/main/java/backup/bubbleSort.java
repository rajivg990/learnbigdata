package backup;

public class bubbleSort {
    static void bubbleSort(int arr []){
        for(int i = 0;i<arr.length-1;i++){
            for(int j  = 0;j<arr.length - i - 1;j++){
                if(arr[j] > arr[j + 1]){
                    int temp = arr[j];
                    arr[j]  = arr[j+1];
                    arr[j + 1] = temp;
                }
            }
        }
    }


    public static void main(String[] args) {
        int arr[] = {2,5,3,9,6,7,4,8};
        bubbleSort(arr);
        for(int i = 0;i<arr.length;i++){
            System.out.print(arr[i] + " ");
        }
    }
}
