package backup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class findPair {
    static void findPair(int [] arr, int sum){
        Map<Integer,Integer> map = new HashMap<>();
        for(int i = 0;i<arr.length;i++){
            if(map.containsKey(sum - arr[i])){
                System.out.println("Pair is " + arr[map.get(sum - arr[i])] + " and " +  arr[i]);
                return;
            }
            else {
                map.put(arr[i],i);
            }
        }
    }
    static void findPairSecond(int arr [],int sum){
        for(int i = 0;i<arr.length -1;i++){
            for(int j = i + 1;i<arr.length ;i++){
                if(arr[i] + arr[j] == sum ){
                    System.out.println(arr[i] + " " + arr[j]);
                }
            }
        }
    }

  //if we have sorted array then we can use 2 pointers

  static void findPairSortedArray(int arr [], int sum){
        int left = 0;
        int right = arr.length - 1;
        while(left < right){
            if(arr[left] + arr[right] > sum){
                right = right - 1;
            }
            if(arr[left] + arr[right] < sum){
                left = left + 1;
            }
            if(arr[left] + arr[right] == sum){
                System.out.println("pair is " + arr[left] + " and " + arr[right]);
                right -= 1;
                left += 1;
            }
        }
  }

    public static void main(String[] args) {
        int [] arr = {2,1,33,-6,9,4};
        int s = -4;
        int sortedArray[]= {1,3,5,7,9,10};
        int search = 8;
        //findPair(arr,s);
        //findPairSecond(arr,s);
        findPairSortedArray(sortedArray,search);
    }
}
