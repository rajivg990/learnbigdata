package backup;

import java.util.HashSet;

public class removeDuplicateFromArray {

    static void removeDuplicateFromArray(int arr []){
        int arr1 [] = new int[arr.length];
        int j = 0;
        for(int i = 0;i<arr.length - 1;i++){
            if(arr[i] != arr[i + 1]){
                arr1[j] = arr[i];
                j++;
            }
        }
        arr1[j] = arr[arr.length - 1];
        for(int i = 0;i<arr1.length;i++){
            System.out.println(arr1[i]);
        }
     }
     //Without using extra arr.
     static void removeDuplicateFromArraySecond(int arr []){
        int j = 0;
        for(int i = 0;i<arr.length - 1;i++){
            if(arr[i] != arr[i+1]){
                arr[j] = arr[i];
                j++;
            }
        }
        arr[j] = arr[arr.length - 1];
        while(j+1 < arr.length){
            arr[j+1] = 0;
            j++;
        }
     }
     //By using hash set it will work for unsorted and sorted array both.
        static void removeDuplicateFromArrayUnsortedArray(int []arr){
            HashSet<Integer> hs = new HashSet<>();
            for(int i = 0;i<arr.length;i++){
                hs.add(arr[i]);
            }
            for(int no : hs){
                System.out.println(no);
            }
        }

    public static void main(String[] args) {
        int arr [] = {1,2,3,3,3,4,4,5,6,6};
       // removeDuplicateFromArray(arr);
      //  removeDuplicateFromArraySecond(arr);
        removeDuplicateFromArrayUnsortedArray(arr);

    }
}
