package array_example;

import java.util.HashSet;

public class commonElementFromTwoArray {
        static void commonElement(int arr [], int arr1[]){
            HashSet<Integer> hs = new HashSet<>();
            for(int no : arr){
                hs.add(no);
            }
            for(int no : arr1){
                boolean b = hs.add(no);
                if(b == false){
                    System.out.println(no);
                }
            }
        }

    public static void main(String[] args) {
        int arr [] = {1,1,3,6,2,7};
        int arr1 [] = {9,5,8,1};

        commonElement(arr,arr1);
    }


}
