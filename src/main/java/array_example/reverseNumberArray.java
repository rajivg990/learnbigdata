package array_example;

public class reverseNumberArray {
    static void reverseNumber(int num){
        int rem,rev = 0;
        while (num != 0){
            rem = num % 10;
            rev = rev * 10 + rem;
            num = num / 10;
        }
        System.out.println(rev);
    }


    public static void main(String[] args) {
        int a = 2342395;
        reverseNumber(a);
    }
}
