package array_example;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DuplicateElementArray {

    static void duplicateElement(int arr[]){
        Map<Integer,Integer> hm = new HashMap<>();
        for(int i : arr){
            Integer count = hm.get(i);
            if(count == null){
                hm.put(i,1);
            }
            else {
                hm.put(i,++count);
            }
        }
        Set<Map.Entry<Integer,Integer>> entrySet = hm.entrySet();
        for(Map.Entry<Integer,Integer> h : entrySet){
            if(h.getValue()>1){
                System.out.println("Duplicate element is " + h.getKey());
            }
        }
    }

    public static void main(String[] args) {

        int arr [] = {1,4,6,3,8,3,1};
        duplicateElement(arr);

    }
}
