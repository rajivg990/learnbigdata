package array_example;

import java.util.HashSet;
import java.util.Set;

public class removeDuplicateArray {

    //This function will work when array is sorted.
    static void removeDuplicateFromSortedArray(int [] arr){
        int [] temp = new int[arr.length];
        int j = 0;
        for(int i = 0;i<arr.length - 1;i++){
            if(arr[i] != arr[i + 1]){
                temp[j] = arr[i];
                j++;
            }
        }
        temp[j] = arr[arr.length - 1];
        for(int i = 0;i<temp.length;i++){
            System.out.println(temp[i]);
        }
    }
    //This will remove duplicste from Sorted and Un-sorted array as well
    // We will be using Hashset, hashset doesnot store duplicate no,
    // HashSet store data in Sorted Manner.
    static void removeDuplicateFromUnsortedArray(int arr []){
        Set<Integer> hs = new HashSet<>();
        for(int i = 0;i<arr.length;i++){
            hs.add(arr[i]);
        }
        for(int i : hs){
            System.out.println(i);
        }
    }
    public static void main(String[] args) {

        int arr[] = {1,3,4,8,9,3,3,4,5,5,6,6,2};
       // removeDuplicateFromSortedArray(arr);
        removeDuplicateFromUnsortedArray(arr);

    }
}
