package array_example;

import scala.Int;

//https://www.youtube.com/watch?v=AGb52zpOBvc
//https://www.youtube.com/watch?v=HgGDf0Ddu8E&list=RDCMUC4C20Zelty7V0fuy5uTHdjQ&index=2

import java.util.HashMap;

public class LongestConsecutiveSequence {

    static int longest_ConsecutiveSequance(int arr[]) {
        HashMap<Integer, Boolean> hm = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            hm.put(arr[i], true);
        }
        for (int i = 0; i < arr.length; i++) {
            if (hm.containsKey(arr[i] - 1)) {
                hm.put(arr[i], false);
            }
        }
        int max = 0;
        for (Integer key : hm.keySet()) {
            if (hm.get(key) == true) {
                max = Math.max(max, findLength(hm, key));
            }
        }
        return max;
    }
    public static int findLength(HashMap<Integer,Boolean> hm, int k) {
            int ans = 0;
            while(hm.containsKey(k)){
                ans++;
                k++;
            }
            return ans;
    }

    public static void main(String[] args) {

        int arr [] = {1,2,6,3,4,7,8,9,10,11};
        int a = longest_ConsecutiveSequance(arr);
        System.out.println(a);

    }
}
