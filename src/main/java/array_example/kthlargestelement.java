package array_example;


//Find kth Largest and Smallest Element in Array Java

public class kthlargestelement {
    static void largestelement(int [] arr, int k){
        for ( int i = 0; i < arr.length; i++ ){
                for (int j = i + 1; j <arr.length - 1; j++){
                    if(arr[i] < arr[j]){
                        int temp = arr[i];
                         arr[i] = arr[j];
                         arr[j] = temp;
                    }

                }
            if(i == k){
                break;
            }
        }
        System.out.println("Kth largest element is " + arr[k-1]);
    }

    public static void main(String[] args) {
        int arr [] = {2,4,3,9,6,7,5};
        largestelement(arr,6);
    }
}
