package array_example;

public class MaximumSumSubArray {
        static void maxSumArray1(int arr []){
            int currentSum = 0;
            int maxSum = Integer.MIN_VALUE;
            int start = 0,end = 0,s = 0;
            for(int i = 0;i<arr.length;i++){
                currentSum = currentSum + arr[i];
                if(currentSum > maxSum){
                    maxSum = currentSum;
                    start = s;
                    end = i;
                }
                if(currentSum < 0){
                    currentSum = 0;
                    s = i + 1;
                }
            }
            System.out.println("Max sum is " + maxSum);
            System.out.println("Staring Index for max subarray " + start + " and Ending index is " + end);
        }

    public static void main(String[] args) {
        int a [] = {5,-4,-2,6,-1};
       maxSumArray1(a);

    }
}
