package array_example;

public class SecondLargestArray {

    static void secondLargest(int [] arr){
        int largest = Integer.MIN_VALUE;
        int second_largest = Integer.MIN_VALUE;

        for(int i = 0;i<arr.length;i++){
            if(arr[i] > largest){
                second_largest = largest;
                largest = arr[i];
            }
            else if(arr[i] > second_largest && arr[i] != largest){
                second_largest = arr[i];
            }
        }
        System.out.println("Second largest element is " + second_largest);
    }

    public static void main(String[] args) {
        int arr [] = {1,4,2,7,5,9};
        secondLargest(arr);
    }
}
