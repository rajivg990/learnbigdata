package linkedList_programs;
import java.io.*;

public class LinkedList
{
    public class Node{
        int data;
        Node next;
    }
     Node head;
    public void insert(int data){
            Node node = new Node();
            node.data = data;
            node.next = null;
            if(head == null){
                head = node;
            }
            else {
                Node n = head;
                while (n.next != null){
                    n = n.next;
                }
                n.next = node;
            }
    }
    public void insertAtStart(int data){
        Node node = new Node();
        node.data = data;
        //node.next = null;
        node.next = head;
        head = node;
    }
    public void insertAt(int index, int data){
        Node node = new Node();
        node.data = data;
        node.next = null;

        if(index == 1){
            insertAtStart(data);
        }
        else {
            Node n = head;
            for (int i = 0; i < index - 1; i++) {
                n = n.next;
            }
            node.next = n.next;
            n.next = node;
        }
    }
    public void show(){
        Node node = head;
        while (node.next != null){
            System.out.println(node.data);
            node = node.next;
        }
        System.out.println(node.data);
    }
    public void delete(int index){
            if(index == 0){
              head = head.next;
            }
            else {
                Node n = head;
                for(int i =0;i<index - 1;i++){
                    n = n.next;
                }
                n.next = n.next.next;
            }
    }
    public void updateAt(int index,int data){
        Node node =  new Node();
        node.data = data;
        Node n = head;
        if(index == 0){
            head.data = data;
        }
        else {
            for (int i = 0; i < index - 1; i++) {
                n = n.next;
            }
            n.next.data = data;
        }
    }

    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.insert(10);
        list.insert(30);
        list.insertAtStart(40);
        list.insertAt(2,22);
        list.show();
        System.out.println("----------------------");
       // list.delete(3);
       // list.show();
        list.updateAt(3,33);
        list.show();

    }

}
