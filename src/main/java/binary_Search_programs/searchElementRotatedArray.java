package binary_Search_programs;

public class searchElementRotatedArray {

//Search an element in Rotated Sorted Array.

    static int SearchElementRotatedArray(int arr[],int x){
       int start = 0;
       int end = arr.length - 1;
       int index = -1;
       while(start <= end){
           int mid = (start + end) / 2;
           int next = (mid + 1) % arr.length;
           int prev = (mid + arr.length - 1) % arr.length;
           if (arr[mid] <= arr[next] && arr[mid] <= arr[prev]){
               index =  mid;
               break;
           }
           else if(arr[mid] >= arr[end]){
               start = mid + 1;
           }
           else if(arr[mid] <= arr[end]){
               end = mid - 1;
           }
       }
    if(x == arr[index]) {
        System.out.println("Element Found at index " + index);
        System.exit(0);
    }

    else{
        if(x > arr[arr.length - 1]){
            start = 0;
            end = index - 1;
        }
        else {
            start = index + 1;
            end = arr.length - 1;
        }
        System.out.println(start);
        System.out.println(end);
        while (start <= end){
            int mid = (start + end) / 2;
            if(arr[mid] == x){
                System.out.println("Element found at index " + mid);
                System.exit(0);
            }
            else if(x <= arr[mid]){
                end = mid - 1;
            }
            else {
                start = mid + 1;
            }
        }
    }
        return -1;
    }

    public static void main(String[] args) {
        int arr [] = {18,19,20,2,5,6,8,9};
        int x = 9;
        int y = SearchElementRotatedArray(arr,x);
        System.out.println(y);


    }
}
