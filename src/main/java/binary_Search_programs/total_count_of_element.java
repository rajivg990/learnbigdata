package binary_Search_programs;

public class total_count_of_element {

// Count no of element in Sorted Array.

    static int count_of_element(int arr [] , int x){
        int start = 0;
        int end = arr.length - 1;
        int first = -1;
        int last = -1;
        while (start <= end) {
            int mid = (start + end) / 2;
            if (arr[mid] == x) {
                first = mid;
                end = mid - 1;
            } else if (x < arr[mid]) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        if (first == -1){
            System.out.println("Element not Found.");
            System.exit(0);
        }
        else {
            start = 0;
            end = arr.length - 1;
            while (start <= end){
                int mid = (start + end)/2;
                if(arr[mid] == x){
                    last = mid;
                    start = mid + 1;
                }
                else if (x < arr[mid]){
                    end = mid - 1;
                }
                else {
                    start = mid + 1;
                }
            }
        }
        return (last - first) + 1;
    }

    public static void main(String[] args) {
        int arr [] = {1,2,3,4,4,4,4,5,6,7};
        int x = 4;
        int y = count_of_element(arr,x);
        System.out.println(y);

    }


}
