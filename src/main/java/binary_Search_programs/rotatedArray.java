package binary_Search_programs;

public class rotatedArray {

        static int NoOfRotation ( int arr[]){
            int start = 0;
            int end = arr.length - 1;

            while (start <= end){
                int mid = (start + end) / 2;
                int next = (mid + 1) % arr.length;
                int prev = (mid + arr.length - 1) % arr.length;
                if(arr[mid] <= arr[prev] && arr[mid] <= arr[next] ){
                    return mid;
                }
                else if(arr[mid]<=arr[end]){
                    end = mid - 1;
                }
                else if(arr[mid]>=arr[start]){
                    start = mid + 1;
                }

            }
            return -1;
        }


    public static void main(String[] args) {

        int arr [] = {1,2,5,6,7,8,3};
        int x = NoOfRotation(arr);
        System.out.println(x);

    }
}
