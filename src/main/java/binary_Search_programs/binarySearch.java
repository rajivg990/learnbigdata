package binary_Search_programs;

public class binarySearch {

// List must be in Sorted Order.
// If there is "Sorted" word in the question, then we have to think how we can apply the binary sesrch.

    static void  BinarySearch1(int arr [], int x){
        int start = 0;
        int end = arr.length - 1;
        while (start <= end){
            int mid = (start + end)/2;
            if(arr[mid] == x){
                System.out.println("Element Found at index " + mid);
                System.exit(0);
            }
            if (arr[mid] < x){
                start = mid + 1;
            }
            else
                end = mid - 1;
        }
        System.out.println("Element Not Found");
    }
   // If we have sorted array in decending order.

    static void decendingOrderBinarySearch(int arr [], int x){
        int start = 0;
        int end = arr.length -1;
        while (start <= end){
            int mid = (start + end)/2;
            if(arr[mid] == x){
                System.out.println("Element found at index " + mid );
                System.exit(0);
            if(x < arr[mid]){
                start = mid + 1;
            }
            else
                end = mid - 1;
            }
        }
      System.out.println("Element not found");
    }


 //     int arr1 [] = {1,3,6,11,14,15,15,15,15};  if need to find of 1st occurance of any element, like here we need to find out 1st and last occurance of element.
    static int firstOccurance(int arr [], int x){
        int start = 0;
        int end = arr.length - 1;
        int first = -1;
        while (start <= end){
            int mid = (start + end)/2;
            if(arr[mid] == x){
               first = mid;
               end = mid - 1;
            }
            else if (x < arr[mid]) {
                end = mid - 1;
            }
            else {
                start = mid + 1;
            }
            }
        return first;
        }
//     int arr1 [] = {1,3,6,11,14,15,15,15,15};  if need to find of 1st occurance of any element, like here we need to find out 1st and last occurance of element.
    static int lastOccurance(int arr[], int x){
        int start = 0;
        int end = arr.length - 1;
        int last = -1;
        while (start <= end){
            int mid = (start + end)/2;
            if(arr[mid] == x){
                last = mid;
                start = mid + 1;
            }
            else if (x < arr[mid]){
                end = mid - 1;
            }
            else {
                start = mid + 1;
            }

        }
        return last;
    }

    public static void main(String[] args) {
     int  arr []  = {2,3,8,11,15,17,22,26,32,47,78};
     int decendingSortedarr [] = {72,65,52,44,39,32,28,22,19,17,11,8};
     int decendingSortedarr1 [] = {10,9,8,7,6,5};
     int n = 14;
    // BinarySearch1(arr,n);
    //decendingOrderBinarySearch(decendingSortedarr1,n);
    int arr1 [] = {1,3,6,11,14,15,15,15,15};
    int x =   firstOccurance(arr1,n)  ;
    System.out.println(x);
    int y = lastOccurance(arr1,n);
    System.out.println(y);


    }


}
