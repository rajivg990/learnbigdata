package binary_Search_programs;

//Nearly Sorted array is like, element which is supposed to be at ith position. means element can be at ith index,i-1 index
//or i + 2 index.

// int arr [] = {5,10,30,20,40}

public class nearlySortedArray {

    static void NearlySortedArray(int arr [], int x){
        int start = 0;
        int end = arr.length - 1;
        while (start <= end){
            int mid = (start + end) / 2;
            if(arr[mid] == x){
                System.out.println("Element found at index " + mid);
                return;
            }
            else if ((mid - 1) >= start && x == arr[mid - 1]){
                System.out.println("Element found at index " + (mid - 1));
                return;
            }
            else if((mid + 1) <= end &&  x == arr[mid + 1]){
                System.out.println("Element found at index " + (mid + 1));
                return;
            }
            else if (x < arr[mid]){
                start = mid + 2;
            }
            else {
                end = mid - 2;
            }
        }
        System.out.println("Element not found");
    }

    public static void main(String[] args) {
        int arr[] = {5,10,30,20,40};
        int x = 10;
        NearlySortedArray(arr,x);

    }
}
