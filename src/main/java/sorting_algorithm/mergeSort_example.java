package sorting_algorithm;

public class mergeSort_example {
    public static void mergeSort(int a[], int beg,int end){
        if(beg < end){
            int mid = (beg + end)/2;
            mergeSort(a,beg,mid);
            mergeSort(a,mid+1,end);
            mergesort(a,beg,mid,end);
        }
    }
    public static void mergesort(int a [] , int l,int mid,int r){
        int i = l;
        int j = mid + 1;
        int k = l;
        int b [] = new int[a.length];
        while (i <= mid && j <= r) {
            if (a[i] < a[j]) {
                b[k] = a[i];
                i++;
            } else {
                b[k] = a[j];
                j++;
            }
            k++;
        }
        if(i > mid){
            while (j<=r){
                b[k] = a[j];
                k++;j++;
            }
        }
        else {
            while (i<=mid){
                b[k] = a[i];
                k++;i++;
            }
        }
        for(k = l;k<=r;k++){
            a[k] = b[k];
        }
    }

    public static void main(String[] args) {

        int a [] = {3,2,1,5,4,7,6};
        mergeSort(a,0,a.length - 1);
        for(int i = 0;i<a.length;i++){
            System.out.println(a[i]);
        }
    }
}
