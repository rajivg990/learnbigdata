package sorting_algorithm;
public class BubbleSort {

   // time complexity is O(n2).
   // space complexity is O(1).

    public static void bubbleSort(int [] arr){
        int temp;
        for(int i = 0;i < arr.length;i++){
            int flag = 0;
            for(int j = 0;j < arr.length - 1 - i; j++){
                if(arr[j] > arr[j + 1]){
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    flag = 1;
                }

            }
            if(flag == 0){
                break;
            }
        }
        for(int i = 0;i <=arr.length -1;i++){
            System.out.println(arr[i] + " ");
        }
    }
    //In Case of String, if(arr[j].compareTo(a[j +1]) > 0), compareTo() methods return 0,-1,1.

    public static void bubbleSort_string(String [] arr){
        String temp;
        for(int i = 0;i < arr.length;i++){
            int flag = 0;
            for(int j = 0;j < arr.length - 1 - i; j++){
                if(arr[j].compareTo(arr[j + 1]) > 0){
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    flag = 1;
                }

            }
            if(flag == 0){
                break;
            }
        }
        for(int i = 0;i <=arr.length -1;i++){
            System.out.println(arr[i] + " ");
        }
    }
    public static void main(String[] args) {
        int arr [] = {36,19,29,12,5};
        String arr1[] = {"Rajiv","Rahul"};
        bubbleSort(arr);
        bubbleSort_string(arr1);
    }
}
