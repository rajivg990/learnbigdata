import sys
def count_of_element(arr,x):
    start = 0
    end = len(arr) - 1
    first = -1
    last = -1
    while(start <= end):
        mid = (start + end) // 2
        if(arr[mid] == x):
            first = mid
            end = mid - 1
        elif(x < arr[mid]):
            end = mid - 1
        else:
            start = mid + 1
    if(first == -1):
        print("Element not Found")
        sys.exit()
    else:
        start = 0
        end = len(arr) - 1
        while(start <= end):
            mid = (start + end) // 2
            if(arr[mid] == x):
                last = mid
                start = mid + 1
            elif(x < arr[mid]):
                end = mid - 1
            else:
                start = mid + 1
    print((last - first) + 1)

arr = [1,2,3,4,4,4,4,5,6]
x = 5
count_of_element(arr,x)

