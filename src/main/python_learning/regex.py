import re


#https://www.youtube.com/watch?v=V_BozMwoYe4

#pattern = "[a-zA-Z0-9]+@+[a-zA-Z]+\.(com|eu|in|net)"

#user_input = input("Please enter your email id \n" )
#if(re.search(pattern,user_input)):
#    print("Valid Email")
#else:
#    print("Invalid Email")


text = 'ABC 123 XYZ 456 @&! 100'

pattern = re.compile(r'\d\d\d')

matches = pattern.search(text)
#print(matches)   # Output = <re.Match object; span=(4, 7), match='123'>

# search method will only look for first possible match and returns that
#if we want to search all the possible match then we can use finditer method.
#finditer returns is an iterator, so we need to use loop.
matches1 = pattern.finditer(text)
#for match in matches1:
#    print(match)

#output
#<re.Match object; span=(4, 7), match='123'>
#<re.Match object; span=(12, 15), match='456'>
#<re.Match object; span=(20, 23), match='100'>

#IF we want to print the exact mathc then we should use group keyword.

matches2 = pattern.search(text).group()
#print(matches2)   # Output = 123

text = '''
Hi, today is 17-Apr-2021,yesterday was 16-Apr-2021 and tomorrow will be 18-Apr-2021.
My schedule is free on 26-04-2021,06.05.2021 and 16/Jun/2021.
You can reach out to me at myname2020@dummy.com or ask_help@demo.net & conferene@demo.co.in
You can also call me at one of the following no's +6032-007 1212, +6099,100 3344, 017-9998800 etc.
'''

datePattern = re.compile(r'\d{2}-[a-zA-Z]{3}-\d{4}')
printDate = datePattern.finditer(text)
for p in printDate:
    print(p.group())

datePattern1 = re.compile(r'\d{2}[-./]([a-zA-Z]{3}|\d{2})[-./]\d{4}')
printDate1 = datePattern1.finditer(text)
for p in printDate1:
    print(p.group())

emailPattern = re.compile(r'\w+@[a-z]+(\.[a-z]{2,3})+')
matchEmail = emailPattern.finditer(text)
for e in matchEmail:
    print(e.group())
