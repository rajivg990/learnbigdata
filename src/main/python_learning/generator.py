def greeting(name):
    yield "Hello " +name
    yield "Welcome " +name
    yield "Have a good day " +name


for a in greeting("Rajiv"):
    print(a)


def even_generators(number):
    for n in number:
        if n % 2 ==0 :
            yield int(n/2)

number = [1,2,3,4,5,6,7,8,9]
eg = even_generators(number)
#print(eg)
#for i in eg:
#    print(i)

