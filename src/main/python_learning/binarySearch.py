import sys
def binarySearch1(arr, x):
 start = 0
 end = len(arr) - 1
 while start <= end:
        mid = (start + end)// 2
        if arr[mid] == x:
            print ("Element is present at index %d" % mid)
            sys.exit()
        elif arr[mid] < x:
            start = mid + 1
        else:
            end = mid - 1
 print ("Element is not present in array")

def binarySearch2(arr, x):
    start = 0
    end = len(arr) - 1
    while start <= end:
        mid = (start + end)// 2
        if arr[mid] == x:
            print ("Element is present at index %d" % mid)
            sys.exit()
        elif x < arr[mid]:
            start = mid + 1
        else:
            end = mid - 1
    print ("Element is not present in array")

# Driver Code
arr = [2, 3, 4, 10, 40]
arr1 = [40, 10, 4, 3, 2]
x = 1

# Function call
#binarySearch1(arr, x)
binarySearch2(arr1, x)