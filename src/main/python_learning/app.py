# Print a value
print("Hello")

# Defining a variable
age = 20
price = 10.5
name = "Rajiv"

# Printing a variable
print(age)

# Taking input from User and printing it.
name1 = input("What is your name?  ")
print("Hello " + name1)