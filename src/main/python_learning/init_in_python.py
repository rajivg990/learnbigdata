class Computer:

 # init is a special constructor which is used to initiate variables.
 # We don't need to call init constructor explicetely. it will get called automatically when object is created.
 # self in parameter belongs to object name. We can give any other name also.
    def __init__(self, cpu, ram):
        self.cpu = cpu
        self.ram = ram


    def config(self):
        print(f"Config is ", self.cpu, self.ram)

com1 = Computer('i5', 16)

com1.config()
print(com1.cpu)