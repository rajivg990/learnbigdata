# You can make a block of code execute over and over again with a while statement.
# The code in a while clause will be executed as long as the while statement’s condition is True.

# You can see that a while statement looks similar to an if statement.
# The difference is in how they behave. At the end of an if clause,
# the program execution continues after the if statement. But at the end of a while clause,
# the program execution jumps back to the start of the while statement.

spam = 0
if spam < 5:
    print("Hello If ")
    spam = spam + 1
print(spam)
print("While Loops begins")
spam1 = 0
while spam1 < 5:
    print(spam1)
    spam1 += 1

# break Statements
# There is a shortcut to getting the program execution to break out of a while loop’s clause early.
# If the execution reaches a break statement, it immedi- ately exits the while loop’s clause.
# In code, a break statement simply contains the break keyword.

print("Example of break statement")
spam2 = 0
while spam2 < 4:
    print(spam2)
    spam2 += 1
    if spam2 == 2:
        break

