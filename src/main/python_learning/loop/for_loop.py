# The while loop keeps looping while its condition is True (which is the reason for its name),
# but what if you want to execute a block of code only a certain number of times?
# You can do this with a for loop statement and the range() function.

for number in range(3):
    print(number)

for x in "Python":
    print(x)

for x in [1, 2, 3, 4]:
    print(x)

shopping_cart = ["Bag", "Chocolate", "Biscuit"]
print(type(shopping_cart))    # it will give which class shopping_card variable is.
for x in shopping_cart:
    print(x)

# exercise  :- Write a program which display an even number from 1 to 10 and print count of even numbers.
print("first exercise")

count = 0
for x in range(1, 10):
    if x % 2 == 0:
        print(x)
        count = count + 1
print(f"We have {count} even numbers.")

# Nested Loops
print("Nested Loop Example")
for x in range(5):
    for y in range(3):
        print(f"{x},{y}")
