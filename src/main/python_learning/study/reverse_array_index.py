import sys

def reverseSorted(arr):
    start = 0
    end = len(arr) - 1
    while(start <= end):
        mid = (start + end) // 2
        next = (mid + 1) % len(arr)
        prev = (mid + len(arr) - 1) % len(arr)
        if (arr[mid] <= arr[next] and arr[mid] <= arr[prev]):
            print(mid)
            sys.exit(0)
        elif(arr[mid] >= arr[end]):
            start = mid + 1
        elif(arr[mid] <= arr[start]):
            end = mid - 1


arr = [18,19,20,2,5,6,8]
reverseSorted(arr)

