try:
    numerator = int(input("Enter the numerator: "))
    denominator = int(input("Enter the denominator: "))

    print(numerator / denominator)
except ZeroDivisionError as e:
    print("denominator cannot be zero")
except:
    print("Please use correcr number")
finally:
    print("This block of code will always run")