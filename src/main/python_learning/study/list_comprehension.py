weekdays = ['sun','mon','tue','wed','thu','fri','sun','mon','mon']
print([[x,weekdays.count(x)] for x in weekdays])

#String Slicing

mystr = "My name is Rajiv"
#print(mystr[0:5])
#print(mystr[0:5:2])

# if we give negative indexing then slicing will take it from right end.
#If we use -1 in the last then it will reverde the sting. so its good example if we want to revese the string.
#print(mystr[::-1])


#print(mystr.count('R'))
#print(mystr.replace("is","are"))


t = (1,)
#print(t)

genius = ["Jerry","Jack","tom","yang"]
l1 = []
for name in genius:
    for char in name:
        l1.append(char)

# Below is the same code using list comprehension.
l2 = [char for name in genius for char in name]

#print(l2)

#print(l1)
l3 = [name for name in genius]

l11 = []
for name in genius:
    if len(name) <4:
        for char in name:
            l11.append(char)

#print(l11)

#Below is same code in list comprehenson
l4 =[char for name in genius if len(name) < 4 for char in name]
#print(l4)


nums = [1,2,3,4]
fruits = ['Mango','Papaya','Grapes','apple']

touples=[(i,j) for i in nums for j in fruits]
#print(touples)
dictt=[{i:j} for i in nums for j in fruits]
#print(dictt)

old_list = [1, 2, 3, 4]
new_list = []

for i in old_list:
    if i%2 == 0:
        new_list.append(i**3)
    else:
        new_list.append(i**2)

#print(new_list)

new_list_comprehension = [i**3 if i % 2 == 0 else i ** 2 for i in old_list ]
#print(new_list_comprehension)


a = [1,2,3,4,5,6]
b = [1,5,6]

commom=[i for i in a if i in b]
#print(commom)

name = ["Rajiv", "Ranjan","Rohit"]
name1 = ["Rajiv"]

common_name = [name for name in name if name in name1]
#print(common_name)
charac="Rajiv"
print(a[::-1])
print(fruits)
print(fruits[::-1])
print(charac[::-1])














