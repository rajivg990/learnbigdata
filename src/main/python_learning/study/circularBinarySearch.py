
def circularBinary(arr,search):
    low = 0
    high=len(arr) - 1;
    while(low <= high):
        mid = int((low + high)/2)
        if (arr[mid] == search):
            return mid
        if (arr[mid] > arr[low]):
            if(search <= arr[low] & search <= arr[mid]):
                high = mid - 1
            else:
                low = mid + 1
        else:
            if(search >= arr[mid] & search <= arr[high]):
                low = mid + 1
            else:
                high = mid - 1

l = [20,30,40,50,60,5,6,7,10]
print(circularBinary(l,7))



