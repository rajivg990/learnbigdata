#Lamda Function also called Anonymous function

def minus(x,y):
    print(x - y)

#Below is the lambda function

minus_lemda = lambda x,y : x - y
#minus(9,5)
#print(minus_lemda(9,5))

triple = lambda x: x*3
#print(triple(10))


grades = [{'name': 'Jennifer', 'final': 95},{'name': 'David', 'final': 92},{'name': 'Aaron', 'final': 98}]

ab=sorted(grades, key=lambda x:x['name'])
#print(ab)

check = [38,24,99,42,2,3,11,23,53,21,3,53,77,12,34,92,122,1008,26]

filt = filter(lambda x : x % 2 == 0, check)
print(list(filt))



