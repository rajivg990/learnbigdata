def moveZeroToEnd(arr):
    count = 0
    for i in range(0,len(arr)):
        if arr[i] != 0:
            arr[count] = arr[i]
            count += 1
    while(count != len(arr)):
        arr[count] = 0
        count += 1

def moveZeroToEnd1(list):
    count = 0
    li = []
    for i in range(0,len(list)):
        if list[i] != 0:
            li.append(list[i])
        else:
            count += 1
    while(count > 0):
        li.append(0)
        count -= 1
    print(li)


arr = [1,3,0,2,0,4]
moveZeroToEnd(arr)
print(arr)


