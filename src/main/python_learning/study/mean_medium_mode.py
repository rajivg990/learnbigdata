pythonic_machine_ages = [19, 22, 34, 26, 32, 30, 24, 24]
after_retirement = [181, 187, 196, 198, 203, 207, 211, 215,230]

def mean(dataset):
    return sum(dataset) / len(dataset)

#print(mean(pythonic_machine_ages))


def median(dataset):
    data = sorted(dataset)
    index = len(dataset) // 2
    if(len(dataset) % 2 != 0):
        return dataset[len(dataset)//2]
    else:
        return (dataset[index -1] + dataset[index]) / 2

#print(median(pythonic_machine_ages))
#print(median(after_retirement))

points_per_game = [3, 15, 23, 42, 30, 10, 13,13,13,12,12]

def mode(dataset):
    frequency = {}
    for value in dataset:
        frequency[value] = frequency.get(value,0) + 1
    most_frequent = max(frequency.values())

    modes = [key for key,value in frequency.items() if value == most_frequent]
    return modes

#print(mode(points_per_game))
names = ['Rajiv','Ranjan','Roohi','Rajiv']


def mode1(dataset):
    frequency = {}
    for value in dataset:
        frequency[value] = frequency.get(value,0) + 1
    ab = [{k:v} for k,v in frequency.items() if v > 1 ]
    print(ab)

#mode1(names)

def mode2(dataset):
    frequency = {}
    for value in dataset:
        frequency[value] = frequency.get(value,0) + 1
    for k,v in frequency.items():
        if v > 1:
            print({k:v})

#mode2(points_per_game)

