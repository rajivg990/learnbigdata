#If we have sortrd array.

def getPair(arr,sum):
    left = 0
    right = len(arr) - 1
    while(left < right):
        if(arr[left] + arr[right] < sum):
            left = left + 1;
        elif(arr[left] + arr[right] > sum):
            right = right - 1;
        elif(arr[left] + arr[right] == sum):
            print("Pair is" ,arr[left] , " and " , arr[right])
            right -= 1
            left += 1

def getPairBruteForch(arr,sum):
    count = 0
    for i in range(len(arr) - 1):
        for j in range(i + 1, len(arr)):
            if(arr[i] + arr[j] == sum):
                #print("Pair is ", arr[i] , " and " , arr[j])
                count = count + 1
    print(count)

arr = [1,1,1,1];
sum=2
#getPair(arr,sum)
getPairBruteForch(arr,sum)
