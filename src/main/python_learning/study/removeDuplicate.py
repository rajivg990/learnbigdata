def removeduplicate(list):
    s = set()
    for i in range(0, len(list)):
        s.add(list[i])
    print(s)

def removeDuplicate1(list):
    for i in range(0, len(list)):
        for j in range(i + 1, len(list) - 1):
            if list[i] == list[j]:
                list.remove(list[i])
    print(list)

list = [1,3,4,8,9,3,4,5,5,6,6,2]
removeduplicate(list)
removeDuplicate1(list)