def reversenumber(number):
    rev = 0
    while (number > 0):
        dig = number % 10
        rev = rev * 10 + dig
        number = number//10
    print(rev)

def reversenumber1(number):
    reverse = 0
    while number != 0:
        reverse = reverse * 10 + number % 10
        number = number // 10
    print(reverse)

reversenumber(123)
reversenumber1(123)