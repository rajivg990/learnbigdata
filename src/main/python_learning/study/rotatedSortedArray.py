import sys
def rotatedArray(arr,x):
     start = 0
     end = len(arr) - 1
     index = -1
     while(start <= end):
         mid = (start + end) //2
         next = (mid + 1) % len(arr)
         prev = (mid + len(arr) - 1) % len(arr)
         if(arr[mid] <= arr[prev] and arr[mid] <= arr[next]):
             index = mid
             break
         elif(arr[mid] <= arr[len(arr) - 1]):
             end = mid - 1
         elif(arr[mid] >= arr[len(arr) - 1]):
             start = mid + 1

     if(index == x):
         print(index)
         sys.exit(0)
     else:
         if(x > arr[(len(arr) - 1)]):
             start = 0
             end = index - 1
         else:
             start = index + 1
             end = len(arr) - 1
         while(start <= end):
            mid = (start + end) // 2
            if(arr[mid] == x):
                print(mid)
                sys.exit(0)
            elif(arr[mid] <= x):
                start = mid + 1
            else:
                end = mid - 1
     print("Element not Found")

arr = [18,19,20,2,5,6,8,9]
x = 25
rotatedArray(arr,x)


