#Exercise 1: Convert two lists into a dictionary

keys = ['Ten', 'Twenty', 'Thirty','Fourty']
values = [10, 20, 30,40]

print(dict(zip(keys,values)))

# zip is used to merge 2 list to disctionary, only matched data will be going in the dictionary.

#Solution 2 It will give error if key and values didnt match

res_disct = dict()
for i in range(len(keys)):
    res_disct.update({keys[i]: values[i]})
print(res_disct)


#Exercise 2: Merge two Python dictionaries into one

dict1 = {'Ten': 10, 'Twenty': 20, 'Thirty': 30}
dict2 = {'Thirty': 30, 'Fourty': 40, 'Fifty': 50}

print({**dict1, **dict2})





