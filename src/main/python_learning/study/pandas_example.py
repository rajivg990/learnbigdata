import pandas as pd
import numpy as np

df = pd.read_csv("/Users/roohi/Documents/pandas/employee_details.csv")
#DF1 = pd.read_excel("")
print(df)
df.plot(x="Company", y="Salary")
#print(df.columns)
#print(df['Salary'])
#print(df.Salary)

#Type of pandas column is of type Pandas SERIES
#print(type(df['Salary']))

#Print multiple columns
#print(df[['Name','Salary']])

#Max salary
#print(df['Salary'].max())

#to print statitas of any DF
#print(df.describe())

#Query Data based on some condition
#print(df[df.Salary > 30000])
#print(df[['Name','Salary','Company']][df.Salary > 30000])


