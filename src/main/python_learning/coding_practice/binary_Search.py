def binarySearch(str,search):
    start = 0
    end = len(str) - 1
    while(start <= end):
        mid = (start + end)//2
        if(str[mid] == search):
            print(f"Number found at index {mid}")
            break
        elif(search >= str[mid]):
            start = mid + 1
        else:
            end = mid - 1
    if(start > end):
        print("Number not found")

l=[1,3,4,5,6,9,10]
s=11
binarySearch(l,s)