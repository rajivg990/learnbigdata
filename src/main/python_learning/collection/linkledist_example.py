class Node:
    def __init__(self, data = None, next = None):   # This is constructor, used to initialize the variable. For every object creation it will get called once.
        self.data = data
        self.next = next

class LinkedList:
    def __init__(self):
        self.head = None

    def insert_at_beginning(self, data):
        node = Node(data, self.head)
        self.head = node

    def print(self):
        if self.head is None:
            print("LinkedList is empty.")
            return

        itr = self.head
        llstr = ''
        while itr:
            llstr += str(itr.data) + '---->'
            itr = itr.next
        print(llstr)

    def insert_at_end(self, data):
        if self.head is None:
            self.head = Node(data, None)
            return
        itr = self.head
        while itr.next:
            itr = itr.next
        itr.next = Node(data, None)

    def insert_values(self, data_list):
        self.head = None
        for data in data_list:
            self.insert_at_end(data)

    def get_length(self):
        count = 0
        itr = self.head
        while itr:
            count += 1
            itr =  itr.next

        return  count

    # Remove an element at given index.
    def remove_at(self, index):
        if index < 0 or index >= self.get_length():
            raise Exception("This is not a valid index.")
        if index == 0:
            self.head = self.head.next
            return
        count = 0
        itr = self.head
        while itr:
            if count == index - 1:
               itr.next = itr.next.next
               break
            itr = itr.next
            count += 1

if __name__ == '__main__':
    ll = LinkedList()
    ll.insert_at_beginning(10)
    ll.insert_at_beginning(89)
    ll.print()
    ll.insert_at_end(79)
    ll.print()

    ll1 = LinkedList()
    ll1.insert_values(['Banana', 'Mango', 'grapes'])
    ll1.print()
    print(ll1.get_length())
    ll1.remove_at(1)
    print(ll1)
