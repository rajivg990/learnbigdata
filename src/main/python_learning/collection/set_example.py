# Un-ordered collection of data with no duplicate.

# Declaration - using curly brackets.

cources = {'History', 'Math', 'Physics', 'Computer','Design'}
print(cources)

cources.add('History')  # Since History is duplicate it will not get added into set.
cources.add('English')  # English is not duplicate so it will be added in set.
print(cources)

cources_1 = {'History', 'Math', 'Physics','Art', 'Computer'}
print(cources)
print(cources.intersection(cources_1))  # It will give element which is available in both sets.
print(cources.union(cources_1))    # It will give all unique element.

# Creating empty Sets.

empty_set = {}     # This is not correct, it will create empty dictionary.
empty_set = set()

