

# Declaration

course = "Python Programming"
print(len(course))      # gives length of String.
print(course[1])        # We can access element of string using index.
print(course[-1])       # We can use negative number to get the element from last.
print(course[:3])       # We can use slicing also in string.


course1 = "Python \" Programming"  # backslash is a special character in strings, it is called escape character.
                                   # This will escape the character after backslash.
print(course1)

# Formatted String
first = "Rajiv"
last = "Gupta"

full = first + " " + last
print(full)
full1 = f"{first} {last}"   # This f is called formatted string. We can put any valid expression in between curly braces.
full2 = f"{len(first)} {last} { 2 + 2}"
print(full1)
print(full2)


print(course.upper())
print(course)
print("Pro" in course)   # It returns boolean value.

print('My name is Simon'.split())  # It splits the strings and return List.
