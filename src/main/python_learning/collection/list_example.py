# Allows to work with sequential data. It stores the data in ordered sequence.
# Lists are mutable while tuples are not.
# We can retrieve list items using index.
# lists and strings are different in an important way.
# A list value is a mutable data type: It can have values added, removed, or changed. However, a string is immutable:
# It cannot be changed.
# When you assign a list to a variable, you are actually assigning a list reference to the variable.
# A reference is a value that points to some bit of data, and a list reference is a value that points to a list.
# Declaration - list begins with an opening square bracket and ends with a closing square bracket, [].


Numbers = [1, 2, 3, 4, 5]
Cource = ['History', 'Math', 'Physics']

# Access data from List

# Using Index
print(Cource[1])     # Index starts from 0.
print(Cource[-1])    # While indexes start at 0 and go up, you can also use negative integers for the index. The integer value -1 refers to the last index in a list.
print(Cource[0: 2])  # We can pass range of index, here range is from 0 to (2 - 1) i.e. 1
print(Cource[1:])    # It will give data from 1st index till end. This is called slicing.
print(Cource[:2])    # It will give data from starting to (2-1) i.e 1st index.

# Add item to list.
Cource.append("Art")
print(Cource)

# Add list to specific location.
Cource.insert(1, "Physics")  # First argument is index location, 2nd argument is element.
print(Cource)

# The append() method call adds the argument to the end of the list.
# The insert() method can insert a value at any index in the list.


# Adding list

cources_2 = ["Che", "Civics"]

Cource.extend(cources_2)  # extend keyword is used to append list.
print(Cource)

# We can use + operatro also to Concatenation of 2 lists.
list_ex = [1, 2, 3] + ['A', 'B', 'C']
print(list_ex)

# Remove from list

Cource.remove("Math")
print(Cource)

Cource.pop()  # Removes (Pooped out) last element from list.
print(Cource)

# Reverse List

Cource.reverse()
print(Cource)

# Sort the List
Cource.sort()
print(Cource)

Cource.sort(reverse=True)
print(Cource)

# Arithmetic functions like min,max,sum is available for list.

# Using loop in list
for course in Cource:
    print(course)

# Access element using index. We can use enumerate function.

for index, course in enumerate(Cource):
    print(index, course)

# Find Index for List
print(Cource.index("History"))

print("Math" in Cource)   # Return boolian value, if element is in list, it will return True

# Convert list into string separated by delimiter.
Cource_str = ','.join(Cource) # Convert the list into string with , delimiter.
print(Cource_str)

# Creating empty List
empty_list = []
empty_list = list()

# We can reassign items in list using index.
Numbers1 = [1, 2, 3, 4, 5]
Numbers1[1] = 6      # It will replace
print(Numbers1)
