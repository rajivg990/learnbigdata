# Data will be in key-value pair. Just like hashmap in java.
# items in dictionaries are unordered.
# Declaration

student = {'name': 'Rajiv', 'age': 20, 'Cources': ['Math', 'Science']}
print(student)
print(student.get('name'))
print(student.get('phone'), 'Not Found')
student['phone'] = '555-5555'
print(student)

age = student.pop('age')
print(age)

print(len(student))  # It will give length of dictionary.

#using loop

for key in student:
    print(key)
for value in student:
    print(value)
for key, value in student.items():
    print(key, value)

picnicItems = {'apples': 5, 'cups': 2}
print('I am bringing ' + str(picnicItems.get('cups', 0)) + ' cups.')
print('I am bringing ' + str(picnicItems.get('eggs', 0)) + ' eggs.')