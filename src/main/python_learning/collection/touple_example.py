# Allows to work with sequential data.
# Tuples are  imm-mutable while list is mutable.
# It is an immutable form of the list data type
# Tuples cannot have their values modified, appended, or removed.
# If you need an ordered sequence of values that never changes, use a tuple.
# Declaration -- using parentheses

tuple_1 = ('History', 'Math', 'Physics', 'Che')
tuple_2 = tuple_1

print(tuple_1)
print(tuple_2)

# tuple_1[0] = 'art'   # Error - TypeError: 'tuple'  object does not support item assignment

print(tuple_1)

# Except immutability others things are same like list.

# Creating Empty touple
empty_touple = ()
empty_touple1 = tuple()


