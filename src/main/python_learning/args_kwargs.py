
def func(normal,*args, **kwargs):
    print(f"This is {normal}")
    for items in args:
        print(items)
    for key, value in kwargs.items():
        print(key,value)


normal = "Normal argument"
ls = ['hi', 'how', 'are', 'you']
dict = {'hi': 'Rajiv', 'hello': 'Ramesh'}

#func(normal, *ls, **dict)

def func1(*args):
    a = 0

    ab = [i for i in args]
    print(ab)

a = 10
b = 11
c = 12

func1(a, b, c)