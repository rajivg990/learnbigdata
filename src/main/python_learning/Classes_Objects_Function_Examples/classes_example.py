# Class is a blueprint

class Employee:
    pass

emp_1 = Employee()
emp_2 = Employee()

print(emp_2)

emp_1.first = "Rajiv"
emp_1.last = "Gupta"

print(emp_1.first)
