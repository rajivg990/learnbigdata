# if you want to create a function and not want to do any thing at that time, we can use pass keyword.
#  By leaving the function black it wont give any error.
def hello_func():
    pass

# calling function
hello_func()

def hello_func_print():
    print("Hey! How are you doing?")

hello_func_print()

def hello(name):
    print(f"Hi  {name}")
hello("Rajiv")

# Passing a default value to function.

def hello1(name = 'You'):
    print(f"Hi {name}")

hello1()
hello1("Ranjan")

# *agrs - returns touple
# **kwargs - returns dictionary
# by using * we can pass multiple tuple, with ** also we can pass multiple arguments.
# args and kwargs are just a name, we can pass any other name.

def student_info(*args, **kwargs):
    print(args)
    print(kwargs)

student_info('Math', 'Art' , name = 'John',age=22)

def funargs(normal, *args, **kwargs):
    print(normal)
    for item in args:
        print(item)
    for key, value in kwargs.items():
       print(f"{key} is a {value}")

har = ["Harry", "Rohan" , "hamad"]
normal = "I am a normal argument."
kw = {"Rohan": "Monitor", "Harry": "Fitness Instructor"}

funargs(normal, *har, **kw)
funargs(normal)   # args and kwargs are options.
funargs(*har)

