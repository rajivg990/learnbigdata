class Employee:
    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first + '.' + last + '@company.com'

    def fullname(self):
        return '{} {}'.format(self.first, self.last)


emp_1 = Employee('Rajiv', 'Gupta', 200000)
emp_2 = Employee("Ranjan", "Gupta", 300000)

print(emp_1.first, emp_1.pay)
print(emp_2.fullname())

print(Employee.fullname(emp_1))


